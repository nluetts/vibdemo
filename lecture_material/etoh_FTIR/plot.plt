set font "Helvetica,48"

set xrange [2700 to 3800]
set yrange [0 to 3.5]
set ytics nomirror
set xtics nomirror
set y2range [0 to 100]
set y2tics 0, 10

set xlabel "wavenumber / cm⁻¹"
set ylabel "absorbance"
set y2label "calculated intensity / km mol⁻¹"

scale = 0.97

set title "FTIR spectrum vs. B3LYP-D4/def2-QZVPPD calculation\n"

plot "trans-etoh.txt"  using (scale*$2):3 with impulses axis x1y2 lw 3 linecolor "red" title "calculation trans", \
     "gauche-etoh.txt" using (scale*$2):3 with impulses axis x1y2 lw 3 dashtype ".." linecolor "blue" title "calculation gauche", \
     "20220214-WB-Stickstoff_40_mbar_EtOH.dpt" with lines axis x1y1 lw 3 linecolor "black" title "spectrum 33 mbar"
