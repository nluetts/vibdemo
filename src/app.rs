use std::collections::HashMap;

use egui::Color32;
use egui_plot::{Arrows, Legend, Line, Plot, PlotPoints, PlotUi, Points};

use crate::{
    physics::{wn_to_period_in_fs, Atom, Field, Molecule},
    Vec2, MYPI,
};

const HELP: &str = "
This program allows you to explore and play with a simple MD simulation of
a triatomic molecule.
                
Manipulate the properties of the molecule (masses, charges,
equilibrium geometry, force constants) to see how they change
the molecule's behavior.

Keyboard controls:

Space: run or pause the simulation
1, 2, 3, 0: drag atom 1 to 3 by clicking the plot (0 to drag the plot)
F: activate the electric field
H: hold the molecule (stop movement and reset acceleration)
P: apply currently selected preset (see Preset menu)
";

#[derive(Debug, PartialEq, Eq)]
enum SelectedAtom {
    None,
    First,
    Second,
    Third,
}

#[derive(Debug, PartialEq, Eq)]
enum Manipulate {
    X,
    Y,
    XY,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Preset {
    CO2,
    CO2Bend,
    CO2SymmetricStretch,
    CO2AntisymmetricStretch,
    H2O,
    H2OBend,
    H2OSymmetricStretch,
    H2OAntisymmetricStretch,
}

#[derive(Debug)]
pub struct App {
    acceleration_buffer: std::collections::VecDeque<f64>,
    play_animation: bool,
    current_preset: Preset,
    field: Field,
    timestep: f64,
    molecule: Molecule,
    selected_atom: SelectedAtom,
    manipulate: Manipulate,
    follow_molecule: bool,
    show_acceleration: bool,
    show_trajectories: bool,
    show_field: bool,
    time: f64,
    normal_freqs_cache: HashMap<String, [f64; 3]>,
    current_normal_freqs: [f64; 3],
}

impl Default for App {
    fn default() -> Self {
        let molecule = Molecule::default();
        Self {
            acceleration_buffer: std::collections::VecDeque::from([1.0; 1000]),
            play_animation: true,
            current_preset: Preset::H2O,
            timestep: 0.1,
            field: Field::default(),
            molecule,
            selected_atom: SelectedAtom::None,
            manipulate: Manipulate::XY,
            follow_molecule: false,
            show_acceleration: true,
            show_trajectories: true,
            show_field: true,
            time: 0.0,
            normal_freqs_cache: HashMap::default(),
            current_normal_freqs: [0.0; 3],
        }
    }
}

impl App {
    fn max_acceleration(&self) -> f64 {
        let max = self
            .acceleration_buffer
            .iter()
            .max_by(|&a, &b| a.total_cmp(b))
            .unwrap_or(&1.0);
        max.to_owned()
    }
    /// keep track of acceleration in the last frames for normalizing color
    /// of acceleration arrows
    fn update_acceleration_buffer(&mut self) {
        let current_max = [
            self.molecule.atm1.acceleration.norm(),
            self.molecule.atm2.acceleration.norm(),
            self.molecule.atm3.acceleration.norm(),
        ]
        .into_iter()
        .max_by(|a, b| a.total_cmp(b))
        .unwrap();
        self.acceleration_buffer.pop_back();
        self.acceleration_buffer.push_front(current_max);
    }
    fn normal_freqs_cached(&mut self) -> &[f64; 3] {
        let eqstate = self.molecule.eqstate();
        let cached = match self.normal_freqs_cache.get(&eqstate) {
            None => false,
            Some(_) => true,
        };
        if !cached {
            self.normal_freqs_cache
                .insert(eqstate.clone(), self.molecule.normal_freqs());
        }
        self.normal_freqs_cache.get(&eqstate).unwrap()
    }
    fn set_field_to_normal_frequency(&mut self, freq: f64) {
        self.field.period = wn_to_period_in_fs(freq);
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.update_acceleration_buffer();
        // repaint constantly to show animation
        ctx.request_repaint();
        egui::TopBottomPanel::top("menu_bar").show(ctx, |ui| self.menu_bar(ui));
        egui::SidePanel::left("left_panel").show(ctx, |ui| self.left_panel(ui));
        egui::CentralPanel::default().show(ctx, |ui| self.plot_panel(ui));
        if self.play_animation {
            for _ in 0..100 {
                let dt = self.timestep / 100.0;
                self.time += dt;
                self.molecule.update(dt, &self.field, self.time);
            }
            self.molecule.update_trajectories(100);
        }
    }
}

// handle key presses
impl App {
    fn key_events(&mut self, ui: &mut egui::Ui) {
        // select atom based on key inputs
        if ui.input(|inp| inp.key_down(egui::Key::Num1)) {
            self.selected_atom = SelectedAtom::First;
        } else if ui.input(|inp| inp.key_down(egui::Key::Num2)) {
            self.selected_atom = SelectedAtom::Second;
        } else if ui.input(|inp| inp.key_down(egui::Key::Num3)) {
            self.selected_atom = SelectedAtom::Third;
        } else if ui.input(|inp| inp.key_down(egui::Key::Num0)) {
            self.selected_atom = SelectedAtom::None;
        }
        // manipulate x, y coordinates sparately
        if ui.input(|inp| inp.key_down(egui::Key::X)) {
            self.manipulate = Manipulate::X;
        } else if ui.input(|inp| inp.key_down(egui::Key::Y)) {
            self.manipulate = Manipulate::Y;
        } else if ui.input(|inp| inp.key_down(egui::Key::Z)) {
            self.manipulate = Manipulate::XY;
        }

        // play/pause on pressing space
        if ui.input(|inp| inp.key_pressed(egui::Key::Space)) {
            self.play_animation = !self.play_animation;
        }
        // activate/deactivate field on pressing space
        if ui.input(|inp| inp.key_pressed(egui::Key::F)) {
            self.field.active = !self.field.active;
        }
        // apply preset on pressing p
        if ui.input(|inp| inp.key_pressed(egui::Key::P)) {
            self.apply_preset(None)
        }
        // halt movement on pressing h
        if ui.input(|inp| inp.key_pressed(egui::Key::H)) {
            self.molecule
                .iter_atoms_mut()
                .for_each(|atm| atm.last_position = atm.position)
        }
    }
}

// Presets
impl App {
    fn apply_preset(&mut self, which: Option<Preset>) {
        let preset = match which {
            None => self.current_preset,
            Some(preset) => preset,
        };
        match preset {
            // harmonic wavenumbers = 675, 1350, 2396 (Suzuki, J. Mol. Spec 1968)
            Preset::CO2 => {
                self.molecule = Molecule::default();
                self.molecule.atm1.mass = 16.0;
                self.molecule.atm2.mass = 12.0;
                self.molecule.atm3.mass = 16.0;
                self.molecule.f12 = 16.0; // Suzuki 1968, CRC handbook
                self.molecule.f13 = 2.4; // Suzuki 1968
                self.molecule.f23 = 16.0;
                self.molecule.fa = 0.78; // Suzuki 1968
                self.molecule.r12_eq = 1.163; // from Wikipedia
                self.molecule.r23_eq = 1.163;
                self.molecule.angle_eq = MYPI;
                let [x1, y1, x2, y2, x3, y3] = self.molecule.equilibrium_coordinates();
                self.molecule.atm1.position = Vec2::new(x1, y1);
                self.molecule.atm2.position = Vec2::new(x2, y2);
                self.molecule.atm3.position = Vec2::new(x3, y3);
                self.molecule.atm1.last_position = Vec2::new(x1, y1);
                self.molecule.atm2.last_position = Vec2::new(x2, y2);
                self.molecule.atm3.last_position = Vec2::new(x3, y3);
                self.field.active = false;
            }
            Preset::CO2Bend => {
                self.apply_preset(Some(Preset::CO2));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[0].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.2,
                    period,
                    angle: 90.0,
                }
            }
            Preset::CO2SymmetricStretch => {
                self.apply_preset(Some(Preset::CO2));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[1].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.2,
                    period,
                    angle: 0.0,
                }
            }
            Preset::CO2AntisymmetricStretch => {
                self.apply_preset(Some(Preset::CO2));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[2].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.2,
                    period,
                    angle: 0.0,
                }
            }
            Preset::H2O => {
                self.molecule = Molecule::default(); // water by default
            }
            Preset::H2OBend => {
                self.apply_preset(Some(Preset::H2O));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[0].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.1,
                    period,
                    angle: 90.0,
                }
            }
            Preset::H2OSymmetricStretch => {
                self.apply_preset(Some(Preset::H2O));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[1].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.1,
                    period,
                    angle: 90.0,
                }
            }
            Preset::H2OAntisymmetricStretch => {
                self.apply_preset(Some(Preset::H2O));
                let period = wn_to_period_in_fs(self.normal_freqs_cached()[2].to_owned());
                self.field = Field {
                    active: true,
                    amplitude: 0.1,
                    period,
                    angle: 0.0,
                }
            }
        }
        self.molecule.stop_motion();
        // empty acceleration buffer
        self.acceleration_buffer.truncate(0);
    }
}

// Menu Bar
impl App {
    fn menu_bar(&mut self, ui: &mut egui::Ui) {
        egui::menu::bar(ui, |ui| {
            // ui.menu_button("Control", |ui| self.control_menu(ui));
            ui.menu_button("View", |ui| self.view_menu(ui));
            ui.menu_button("Presets", |ui| self.preset_menu(ui));
            ui.menu_button("Field", |ui| self.field_menu(ui));
            ui.menu_button("Manipulate", |ui| self.manipulate_menu(ui));
            ui.menu_button("Help", |ui| self.help_menu(ui));
        });
    }
    fn control_menu(&mut self, ui: &mut egui::Ui) {}
    fn view_menu(&mut self, ui: &mut egui::Ui) {
        ui.toggle_value(&mut self.show_acceleration, "Show Acceleration");
        ui.toggle_value(&mut self.show_trajectories, "Show Trajectories");
        ui.toggle_value(&mut self.show_field, "Show Field");
        ui.toggle_value(&mut self.follow_molecule, "Follow Molecule");
    }
    fn preset_menu(&mut self, ui: &mut egui::Ui) {
        ui.radio_value(&mut self.current_preset, Preset::CO2, "CO₂");
        ui.radio_value(&mut self.current_preset, Preset::CO2Bend, "CO₂ bend");
        ui.radio_value(
            &mut self.current_preset,
            Preset::CO2SymmetricStretch,
            "CO₂ Symmetric Stretch",
        );
        ui.radio_value(
            &mut self.current_preset,
            Preset::CO2AntisymmetricStretch,
            "CO₂ Anti-Symmetric Stretch",
        );
        ui.radio_value(&mut self.current_preset, Preset::H2O, "H₂O");
        ui.radio_value(&mut self.current_preset, Preset::H2OBend, "H₂O bend");
        ui.radio_value(
            &mut self.current_preset,
            Preset::H2OSymmetricStretch,
            "H₂O Symmetric Stretch",
        );
        ui.radio_value(
            &mut self.current_preset,
            Preset::H2OAntisymmetricStretch,
            "H₂O Anti-Symmetric Stretch",
        );
        if ui.button("Apply Preset (P)").clicked() {
            self.apply_preset(None);
        }
    }
    fn field_menu(&mut self, ui: &mut egui::Ui) {
        if ui.checkbox(&mut self.field.active, "Active? (F)").clicked() {
            // reset field timer
            self.time = 0.0
        };
        ui.add(egui::Slider::new(&mut self.field.amplitude, 0.0..=1.0).text("Amplitude"));
        ui.add(
            egui::Slider::new(&mut self.field.period, 0.0..=200.0)
                .text("Period")
                .suffix(" fs"),
        );
        ui.add(
            egui::Slider::new(&mut self.field.angle, 0.0..=90.0)
                .text("Direction")
                .suffix("°"),
        );
        ui.label("set to normal frequency");
        for wn in self.normal_freqs_cached().to_owned() {
            if ui.button(format!("{:.5}", wn)).clicked() {
                self.set_field_to_normal_frequency(wn);
            };
        }
    }
    fn manipulate_menu(&mut self, ui: &mut egui::Ui) {
        egui::Frame::none().show(ui, |ui| {
            ui.radio_value(
                &mut self.selected_atom,
                SelectedAtom::None,
                "None (Drag Plot)",
            );
            ui.radio_value(&mut self.selected_atom, SelectedAtom::First, "Atom 1");
            ui.radio_value(&mut self.selected_atom, SelectedAtom::Second, "Atom 2");
            ui.radio_value(&mut self.selected_atom, SelectedAtom::Third, "Atom 3");
            ui.radio_value(&mut self.manipulate, Manipulate::X, "Just X");
            ui.radio_value(&mut self.manipulate, Manipulate::Y, "Just Y");
            ui.radio_value(&mut self.manipulate, Manipulate::XY, "X and Y");
        });
        if ui.button("Halt Movement (H)").clicked() {
            self.molecule.stop_motion();
        }
    }
    fn help_menu(&mut self, ui: &mut egui::Ui) {
        ui.set_min_width(500.0);
        ui.heading("Hooke's Molecule");
        let text = egui::text::LayoutJob::single_section(
            HELP.to_owned(),
            egui::TextFormat {
                ..Default::default()
            },
        );
        ui.label(text);
        ui.heading("Debug Info");
        ui.label(format!(
            "energy = {:.2}",
            self.molecule.kinetic_energy() + self.molecule.potential_energy()
        ));
    }
}

impl App {
    fn left_panel(&mut self, ui: &mut egui::Ui) {
        ui.heading("masses");
        ui.add(egui::Slider::new(&mut self.molecule.atm1.mass, 1.0..=1e6).text("m₁"));
        ui.add(egui::Slider::new(&mut self.molecule.atm2.mass, 1.0..=1e6).text("m₂"));
        ui.add(egui::Slider::new(&mut self.molecule.atm3.mass, 1.0..=1e6).text("m₃"));
        ui.heading("charges");
        ui.add(egui::Slider::new(&mut self.molecule.atm1.charge, -3.0..=3.0).text("q₁"));
        ui.add(egui::Slider::new(&mut self.molecule.atm2.charge, -3.0..=3.0).text("q₂"));
        ui.add(egui::Slider::new(&mut self.molecule.atm3.charge, -3.0..=3.0).text("q₃"));
        ui.heading("equilibrium geometry");
        ui.add(egui::Slider::new(&mut self.molecule.r12_eq, 0.0..=4.0).text("r₁₂eq"));
        ui.add(egui::Slider::new(&mut self.molecule.r23_eq, 0.0..=4.0).text("r₂₃eq"));
        // TODO: if I limit the max value of the angle to PI, I get NaNs in the hessian
        ui.add(egui::Slider::new(&mut self.molecule.angle_eq, 0.0..=MYPI).text("γeq"));
        ui.heading("force constants");
        ui.add(egui::Slider::new(&mut self.molecule.f12, 0.0..=30.0).text("f₁₂"));
        ui.add(egui::Slider::new(&mut self.molecule.f13, 0.0..=30.0).text("f₁₃"));
        ui.add(egui::Slider::new(&mut self.molecule.f23, 0.0..=30.0).text("f₂₃"));
        ui.add(egui::Slider::new(&mut self.molecule.fa, 0.0..=30.0).text("fγ"));
        ui.heading("simulation controls");
        let dt_label = egui::text::LayoutJob::single_section(
            "Timestep per Frame (fs)".to_owned(),
            egui::TextFormat {
                ..Default::default()
            },
        );
        ui.label(dt_label);
        ui.add(egui::Slider::new(&mut self.timestep, 1e-2..=1.0).logarithmic(true));
        if ui.button("Reset").clicked() {
            self.apply_preset(None);
            self.time = 0.0;
            self.field = Field::default();
            self.acceleration_buffer.clear();
        }
        if self.play_animation {
            let btn =
                egui::widgets::Button::new("⏸ Pause (Space)").fill(Color32::from_rgb(200, 0, 0));
            if ui.add(btn).clicked() {
                self.play_animation = false;
            }
        } else {
            let btn = egui::widgets::Button::new("▶ Run (Space)")
                .fill(Color32::from_rgb(0, 100, 0))
                .min_size(egui::Vec2::new(100.0, 20.0));
            if ui.add(btn).clicked() {
                self.play_animation = true;
            }
        }
        ui.heading("normal frequencies");
        for freq in self.normal_freqs_cached().to_owned() {
            ui.label(format!(
                "{:.1} cm¯¹ (T = {:.2} fs)",
                freq,
                1e15 / (299792458.0 * freq * 100.0)
            ));
        }
    }
}

// Plotting
impl App {
    fn mouse_manipulation(&mut self, pt: egui_plot::PlotPoint) {
        if self.follow_molecule {
            // manipulation of atoms does only work if plotting area is not moving
            return;
        }
        let atom: Option<&mut Atom> = match self.selected_atom {
            SelectedAtom::None => None,
            SelectedAtom::First => Some(&mut self.molecule.atm1),
            SelectedAtom::Second => Some(&mut self.molecule.atm2),
            SelectedAtom::Third => Some(&mut self.molecule.atm3),
        };
        if let Some(atom) = atom {
            atom.velocity.x = 0.0;
            atom.velocity.y = 0.0;
            atom.acceleration.x = 0.0;
            atom.acceleration.y = 0.0;
            match self.manipulate {
                Manipulate::X => atom.position.x = pt.x,
                Manipulate::Y => atom.position.y = pt.y,
                Manipulate::XY => {
                    atom.position.x = pt.x;
                    atom.position.y = pt.y;
                }
            }
            // reset last position, otherwise Velet algorithm explodes
            atom.last_position = atom.position;
            atom.trajectory.clear();
        }
    }
    fn plot_panel(&mut self, ui: &mut egui::Ui) {
        // react to key-events
        self.key_events(ui);
        let allow_drag = match self.selected_atom {
            SelectedAtom::None => true,
            _ => false,
        };
        let origin = if self.follow_molecule {
            self.molecule.center_of_gravity()
        } else {
            Vec2::default()
        };
        let plot = Plot::new("A Molecule")
            .legend(Legend::default())
            .data_aspect(1.0)
            .set_margin_fraction((0.1, 0.1).into())
            .include_x(-5.0 + origin.x)
            .include_x(5.0 + origin.x)
            .include_y(-5.0 + origin.y)
            .include_y(5.0 + origin.y)
            .allow_drag(allow_drag && !self.follow_molecule);
        let plot = if self.follow_molecule {
            plot.reset()
        } else {
            plot
        };
        plot.show(ui, |plot_ui| {
            // molecule
            let graph: Vec<[f64; 2]> = vec![
                self.molecule.atm1.position.into(),
                self.molecule.atm2.position.into(),
                self.molecule.atm3.position.into(),
            ];
            // trajectories
            if self.show_trajectories {
                self.molecule.atm1.plot_trajectory(plot_ui, None);
                self.molecule.atm2.plot_trajectory(plot_ui, None);
                self.molecule.atm3.plot_trajectory(plot_ui, None);
            }
            // mouse action
            if plot_ui.response().is_pointer_button_down_on() {
                match plot_ui.pointer_coordinate() {
                    Some(pt) => self.mouse_manipulation(pt),
                    None => (),
                }
            }
            plot_ui.line(
                Line::new(PlotPoints::from(graph.clone()))
                    .width(5.0)
                    .name("XYZ"),
            );
            plot_ui.points(
                Points::new(PlotPoints::from(graph))
                    .name("XYZ")
                    .radius(20.0),
            );
            // field
            if self.field.active && self.show_field {
                let acc = self.field.acceleration(1.0, self.time);
                let origins: Vec<[f64; 2]> = (-10..10)
                    .step_by(2)
                    .into_iter()
                    .map(|x| [x as f64, 0.0])
                    .collect();
                let tips: Vec<[f64; 2]> = origins
                    .iter()
                    .map(|pt| [acc.x + pt[0], acc.y + pt[1]])
                    .collect();
                let arrow = Arrows::new(origins, tips).color(Color32::from_white_alpha(127));
                plot_ui.arrows(arrow);
            };
            if self.show_acceleration {
                let maxa = self.max_acceleration();
                let arrow = Arrows::new(
                    vec![
                        self.molecule.atm2.position.into(),
                        self.molecule.atm1.position.into(),
                        self.molecule.atm3.position.into(),
                    ],
                    vec![
                        (self.molecule.atm2.acceleration / maxa + self.molecule.atm2.position)
                            .into(),
                        (self.molecule.atm1.acceleration / maxa + self.molecule.atm1.position)
                            .into(),
                        (self.molecule.atm3.acceleration / maxa + self.molecule.atm3.position)
                            .into(),
                    ],
                )
                .color(Color32::from_rgb(0, 255, 0));
                plot_ui.arrows(arrow);
            }
        });
    }
}

impl Atom {
    fn plot_trajectory(&self, plot_ui: &mut PlotUi, color: Option<Color32>) {
        let pts = PlotPoints::from_iter(self.trajectory.iter().map(|pt| pt.to_owned().into()));
        plot_ui.line(Line::new(pts).color(Color32::from_white_alpha(255)));
    }
}
