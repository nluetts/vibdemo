#![allow(dead_code, unused_variables)]
use nalgebra::Vector2;

// approximate pi that still works with the potential function
// (non NaN in normal frequencies, etc.)
// true pi      = 3.1415926535897932384626433...
const MYPI: f64 = 3.1415926;
type Vec2 = Vector2<f64>;

mod app;
mod physics;
pub use app::App;
pub use physics::{Field, Molecule};
