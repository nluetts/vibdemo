use crate::Vec2;
use nalgebra::{Matrix6, SVector};
use std::collections::VecDeque;
use std::f64::consts::PI;

use num_dual::*;

const C0: f64 = 299792458.0; // speed of light in vacuum in m/s

#[derive(Debug)]
pub struct Field {
    pub active: bool,
    pub amplitude: f64,
    pub period: f64,
    /// from 0.0 to 360.0 degrees
    pub angle: f64,
}

impl Default for Field {
    fn default() -> Self {
        Field {
            active: false,
            amplitude: 0.5,
            period: 50.0,
            angle: 90.0,
        }
    }
}

impl Field {
    fn efield(&self, time: f64) -> Vec2 {
        let x = (self.angle / 360.0 * 2.0 * PI).cos();
        let y = (self.angle / 360.0 * 2.0 * PI).sin();
        let direction = Vec2::new(x, y);
        direction * self.amplitude * (2.0 * PI / self.period * time).sin()
    }
    pub fn acceleration(&self, charge: f64, time: f64) -> Vec2 {
        self.efield(time) * charge
    }
}

type Position = Vec2;
type Velocity = Vec2;
type Acceleration = Vec2;
type Trajectory = VecDeque<Vec2>;

#[derive(Debug)]
pub struct Atom {
    pub mass: f64,
    pub charge: f64,
    pub color: (u8, u8, u8),
    pub position: Position,
    pub last_position: Position,
    pub velocity: Velocity,
    pub acceleration: Acceleration,
    pub trajectory: Trajectory,
}

impl Atom {
    fn velet_step(&mut self, force: Vec2, timestep: f64) -> Vec2 {
        // unit conversion
        let force = force * 1e-8; // from aJ/Å to N
        let timestep = timestep * 1e-15; // from ps to seconds
        let mass = self.mass * 1.6605390666e-27; // from amu to kg
        let forward_step = force * timestep.powi(2) / mass * 1e10; // from m to Å
        self.position * 2.0 - self.last_position + forward_step
    }
}

#[derive(Debug)]
pub struct Molecule {
    pub atm1: Atom,
    pub atm2: Atom,
    pub atm3: Atom,
    pub f12: f64,
    pub f13: f64,
    pub f23: f64,
    pub fa: f64,
    pub r12_eq: f64,
    pub r23_eq: f64,
    pub angle_eq: f64,
}

impl Default for Molecule {
    fn default() -> Self {
        let atm1 = Atom {
            mass: 1.0,
            charge: 0.25,
            color: (200, 200, 200),
            position: Vec2::default(),
            last_position: Vec2::default(),
            velocity: Vec2::default(),
            acceleration: Vec2::default(),
            trajectory: VecDeque::with_capacity(1000),
        };
        let atm2 = Atom {
            mass: 16.0,
            charge: -0.5,
            color: (255, 0, 0),
            position: Vec2::default(),
            last_position: Vec2::default(),
            velocity: Vec2::default(),
            acceleration: Vec2::default(),
            trajectory: VecDeque::with_capacity(1000),
        };
        let atm3 = Atom {
            mass: 1.0,
            charge: 0.25,
            color: (200, 200, 200),
            position: Vec2::default(),
            last_position: Vec2::default(),
            velocity: Vec2::default(),
            acceleration: Vec2::default(),
            trajectory: VecDeque::with_capacity(1000),
        };
        let mut mol = Molecule {
            atm2,
            atm1,
            atm3,
            f12: 8.0,
            f13: 0.5,
            f23: 8.0,
            fa: 0.6,
            r12_eq: 0.957,
            r23_eq: 0.957,
            angle_eq: 1.824,
        };
        let [x1, y1, x2, y2, x3, y3] = mol.equilibrium_coordinates();
        mol.atm1.position = Vec2::new(x1, y1 + 0.5);
        mol.atm2.position = Vec2::new(x2, y2 + 0.5);
        mol.atm3.position = Vec2::new(x3, y3 + 0.5);
        mol.atm1.last_position = Vec2::new(x1, y1 + 0.5);
        mol.atm2.last_position = Vec2::new(x2, y2 + 0.5);
        mol.atm3.last_position = Vec2::new(x3, y3 + 0.5);
        mol
    }
}

impl Molecule {
    /// Perform a timestep and update molecular structure
    pub fn update(&mut self, dt: f64, field: &Field, time: f64) {
        // update forces
        let (_, mut forces) = num_dual::gradient(
            |v| self.potential_energy_dual(v[0], v[1], v[2], v[3], v[4], v[5]),
            SVector::from(self.cartesian_coordiantes()),
        );
        if field.active {
            let efield = field.efield(time);
            forces[0] += self.atm1.charge * efield.x;
            forces[1] += self.atm1.charge * efield.y;
            forces[2] += self.atm2.charge * efield.x;
            forces[3] += self.atm2.charge * efield.y;
            forces[4] += self.atm3.charge * efield.x;
            forces[5] += self.atm3.charge * efield.y;
        }
        let f1 = Vec2::new(-forces[0], -forces[1]);
        let f2 = Vec2::new(-forces[2], -forces[3]);
        let f3 = Vec2::new(-forces[4], -forces[5]);

        // update acceleration (for drawing only)
        self.atm1.acceleration = f1 / self.atm1.mass;
        self.atm2.acceleration = f2 / self.atm2.mass;
        self.atm3.acceleration = f3 / self.atm3.mass;

        // update position
        let next_position1 = self.atm1.velet_step(f1, dt);
        let next_position2 = self.atm2.velet_step(f2, dt);
        let next_position3 = self.atm3.velet_step(f3, dt);
        self.atm1.last_position = self.atm1.position;
        self.atm2.last_position = self.atm2.position;
        self.atm3.last_position = self.atm3.position;
        self.atm1.position = next_position1;
        self.atm2.position = next_position2;
        self.atm3.position = next_position3;
    }
    pub fn update_trajectories(&mut self, n: usize) {
        [&mut self.atm1, &mut self.atm2, &mut self.atm3]
            .into_iter()
            .for_each(|atm| {
                atm.trajectory.push_front(atm.position);
                while atm.trajectory.len() > n {
                    atm.trajectory.pop_back();
                }
            });
    }
    pub fn stop_motion(&mut self) {
        self.iter_atoms_mut().for_each(|atm| {
            atm.last_position = atm.position;
        });
    }
    fn iter_atoms(&self) -> std::array::IntoIter<&Atom, 3> {
        [&self.atm1, &self.atm2, &self.atm3].into_iter()
    }
    pub fn iter_atoms_mut(&mut self) -> std::array::IntoIter<&mut Atom, 3> {
        [&mut self.atm1, &mut self.atm2, &mut self.atm3].into_iter()
    }
    /// bond distance oxygen 1 <-> hydrogen 2
    fn r12(&self) -> f64 {
        (self.atm2.position - self.atm1.position).norm()
    }
    /// unit vector in direction of bond 1 -> 2
    fn e12(&self) -> Vec2 {
        (self.atm2.position - self.atm1.position).normalize()
    }
    /// bond distance oxygen 1 <-> hydrogen 3
    fn r23(&self) -> f64 {
        (self.atm2.position - self.atm3.position).norm()
    }
    /// unit vector in direction of bond 1 -> 3
    fn e23(&self) -> Vec2 {
        (self.atm2.position - self.atm3.position).normalize()
    }
    /// bond distance hydrogen 2 <-> hydrogen 3
    fn r13(&self) -> f64 {
        (self.atm1.position - self.atm3.position).norm()
    }
    /// unit vector in direction of bond 2 -> 3
    fn e13(&self) -> Vec2 {
        (self.atm1.position - self.atm3.position).normalize()
    }
    fn angle(&self) -> f64 {
        let p12sq = self.r12().powi(2);
        let p23sq = self.r23().powi(2);
        let p13sq = self.r13().powi(2);
        let inner = (p12sq + p23sq - p13sq) / (2.0 * p12sq.sqrt() * p23sq.sqrt());
        inner.acos()
    }
    fn print_inner_coordinates(&self) {
        println!(
            "r12 = {}, r23 = {}, r13 = {}, angle = {}",
            self.r12(),
            self.r23(),
            self.r13(),
            self.angle()
        );
    }
    fn cartesian_coordiantes(&self) -> [f64; 6] {
        [
            self.atm1.position.x,
            self.atm1.position.y,
            self.atm2.position.x,
            self.atm2.position.y,
            self.atm3.position.x,
            self.atm3.position.y,
        ]
    }
    pub fn equilibrium_coordinates(&self) -> [f64; 6] {
        // the middle atom is at coordinate system origin
        let x2 = 0.0;
        let y2 = 0.0;
        let a = self.r12_eq;
        let b = self.r23_eq;
        let c = (a.powi(2) + b.powi(2) - 2.0 * a * b * self.angle_eq.cos()).sqrt();
        // the three atoms form a triangle where the side c (r13) is parallel
        // to the x-axis; h is the height of this triangle and points "down"
        let h = ((a + b - c) * (a - b + c) * (-a + b + c) * (a + b + c)).sqrt() / (2.0 * c);
        let x1 = -(a.powi(2) - h.powi(2)).sqrt();
        let y1 = -h;
        let x3 = (b.powi(2) - h.powi(2)).sqrt();
        let y3 = -h;
        [x1, y1, x2, y2, x3, y3]
    }
    pub fn kinetic_energy(&self) -> f64 {
        [&self.atm1, &self.atm2, &self.atm3]
            .into_iter()
            .map(|a| a.velocity.norm_squared() * a.mass * 0.5)
            .sum()
    }
    pub fn potential_energy(&self) -> f64 {
        let [x1, y1, x2, y2, x3, y3] = self.cartesian_coordiantes();
        self.potential_energy_dual(x1, y1, x2, y2, x3, y3)
    }
    pub fn center_of_gravity(&self) -> Vec2 {
        let mass: f64 = self.iter_atoms().map(|atm| atm.mass).sum();
        let x0 = self
            .iter_atoms()
            .map(|atm| atm.mass * atm.position.x)
            .sum::<f64>()
            / mass;
        let y0 = self
            .iter_atoms()
            .map(|atm| atm.mass * atm.position.y)
            .sum::<f64>()
            / mass;
        Vec2::new(x0, y0)
    }
}

pub fn angle_dual<D: DualNum<f64>>(a: D, b: D, c: D) -> D {
    let cosg = (a.powi(2) + b.powi(2) - c.powi(2)) / (a * b * 2.0);
    cosg.acos()
}
pub fn angle_f64(a: f64, b: f64, c: f64) -> f64 {
    let cosg = (a.powi(2) + b.powi(2) - c.powi(2)) / (a * b * 2.0);
    cosg.acos()
}

impl Molecule {
    pub fn potential_energy_dual<D: DualNum<f64> + Copy>(
        &self,
        x1: D,
        y1: D,
        x2: D,
        y2: D,
        x3: D,
        y3: D,
    ) -> D {
        let r12 = ((x2 - x1).powi(2) + (y2 - y1).powi(2)).sqrt();
        let r13 = ((x3 - x1).powi(2) + (y3 - y1).powi(2)).sqrt();
        let r23 = ((x3 - x2).powi(2) + (y3 - y2).powi(2)).sqrt();
        let gamma = {
            let a = r12;
            let b = r23;
            let c = r13;
            let cosg = (a.powi(2) + b.powi(2) - c.powi(2)) / (a * b * 2.0);
            // this guards against NaNs from spurious floating point errors
            if cosg.re() <= -1.0 {
                PI.into()
            } else if cosg.re() >= 1.0 {
                0.0.into()
            } else {
                cosg.acos()
            }
        };
        let term12 = (r12 - self.r12_eq).powi(2) * self.f12;
        let term13 = (r12 - self.r12_eq) * (r23 - self.r23_eq) * self.f13;
        let term23 = (r23 - self.r23_eq).powi(2) * self.f23;
        let term_angle = if gamma == self.angle_eq.into() {
            0.0.into()
        } else {
            (gamma - self.angle_eq).powi(2) * self.fa
        };
        (term12 + term13 * 2.0 + term23 + term_angle) * 0.5
    }
    fn hessian(&self, qs: [f64; 6]) -> Matrix6<f64> {
        let (_, _, hess) = hessian(
            |v| self.potential_energy_dual(v[0], v[1], v[2], v[3], v[4], v[5]),
            SVector::from(qs),
        );
        hess.into()
    }
    pub fn normal_freqs(&self) -> [f64; 3] {
        let mut hess = self.hessian(self.equilibrium_coordinates());
        let masses = [
            self.atm1.mass,
            self.atm1.mass,
            self.atm2.mass,
            self.atm2.mass,
            self.atm3.mass,
            self.atm3.mass,
        ]
        .map(|m| m * 1.6605390666e-27); // multiply with atomic mass unit in kg

        // weight elements by 1/sqrt(mass_i × mass_j)
        // and convert to SI units
        for i in 0..6 {
            for j in 0..6 {
                // unit of hessian elements is aJ/Å² = 100 N/m
                hess[(i, j)] *= 100.0 * (masses[i] * masses[j]).sqrt().recip()
            }
        }

        // calculate frequencies
        let eigvalues = hess.symmetric_eigen().eigenvalues;
        let mut eigvalues: Vec<f64> = eigvalues
            .iter()
            .filter_map(|x| {
                // calculate frequency in inverse centimeters
                // factor 100.0 converts from 1/m to 1/cm
                let freq = x.sqrt() / (100.0 * 2.0 * PI * C0);
                if freq.is_nan() || freq < 10.0 {
                    None
                } else {
                    Some(freq)
                }
            })
            .collect();
        eigvalues.sort_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Less));
        TryInto::<[f64; 3]>::try_into(eigvalues).unwrap_or_default()
    }
    pub fn eqstate(&self) -> String {
        let eqconf = [
            self.atm1.mass,
            self.atm2.mass,
            self.atm3.mass,
            self.atm1.charge,
            self.atm2.charge,
            self.atm3.charge,
            self.r12_eq,
            self.r23_eq,
            self.angle_eq,
            self.f12,
            self.f13,
            self.f23,
            self.fa,
        ];
        format!("{:#?}", eqconf)
    }
}

pub fn wn_to_period_in_fs(wn: f64) -> f64 {
    (wn * 100.0 * C0).recip() * 1e15
}
