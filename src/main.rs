#![allow(dead_code, unused_variables)]

use std::f64::consts::PI;

use eframe::egui;
use vibdemo::{App, Field, Molecule};

#[cfg(not(target_arch = "wasm32"))]
fn main() -> Result<(), eframe::Error> {
    // debug();
    // return Ok(());

    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(800.0, 600.0)),
        follow_system_theme: false,
        default_theme: eframe::Theme::Dark,
        ..Default::default()
    };
    eframe::run_native(
        "Hooke's Molecule",
        options,
        Box::new(|cc| Box::<App>::default()),
    )
}

#[cfg(target_arch = "wasm32")]
fn main() {
    // debug();
    // return Ok(());
    let web_options = eframe::WebOptions {
        follow_system_theme: false,
        default_theme: eframe::Theme::Dark,
        ..eframe::WebOptions::default()
    };

    wasm_bindgen_futures::spawn_local(async {
        eframe::WebRunner::new()
            .start(
                "the_canvas_id", // hardcode it
                web_options,
                Box::new(|cc| Box::<App>::default()),
            )
            .await
            .expect("failed to start eframe");
    });
}

fn debug() {
    let mut mol = Molecule {
        angle_eq: PI,
        r12_eq: 1.4,
        r23_eq: 1.4,
        ..Default::default()
    };
    let [x1, y1, x2, y2, x3, y3] = mol.equilibrium_coordinates();
    mol.atm1.position.x = x1;
    mol.atm1.position.y = y1;
    mol.atm2.position.x = x2;
    mol.atm2.position.y = y2;
    mol.atm3.position.x = x3;
    mol.atm3.position.y = y3;

    let timestep = 1e-3;
    let mut time = 0.0;
    let mut field = Field::default();
    field.angle = 0.0;
    field.active = true;
    loop {
        mol.update(timestep, &field, time);
        time += timestep;
    }
}
