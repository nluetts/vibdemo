build-wasm:
	trunk build --release --public-url data/ilias3/lm_data/lm_224781/

build-CO2-html:
	jupyter-nbconvert --to slides --TagRemovePreprocessor.remove_input_tags='{"hide_code"}' lecture_material/CO2-1D/CO2-1D.ipynb
	mv lecture_material/CO2-1D/CO2-1D.slides.html lecture_material/CO2-1D/CO2-1D-slides.html
